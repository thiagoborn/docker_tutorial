package org.born;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import spark.Spark;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import static java.lang.System.getenv;
import static java.util.stream.Collectors.joining;
import static spark.Spark.get;

public class App {
    public static void main(String[] args) throws Exception {

        var logger = LoggerFactory.getLogger("env-server");

        logger.info("starting 2222");
        var hostName = InetAddress.getLocalHost().getHostName();

        var servicePort = Integer.parseInt(getenv().getOrDefault("SERVICE_PORT", "4567"));
        Spark.port(servicePort);
        logger.info("service port is " + servicePort);

        logger.info("starting metrics");
        String graphiteHost = getenv().getOrDefault("GRAPHITE_HOST", "localhost");
        logger.info("graphite host is " + graphiteHost);
        var graphite = new Graphite(graphiteHost, 2003);
        var registry = new MetricRegistry();
        var reporter = GraphiteReporter.forRegistry(registry)
                .prefixedWith(getenv().getOrDefault("METRICS_PREFIX", hostName))
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(MetricFilter.ALL)
                .build(graphite);
        reporter.start(1, TimeUnit.MINUTES);
        logger.info("metrics started");

        logger.info("loading redis pool");
        String redisHost = getenv().getOrDefault("REDIS_HOST", "localhost");
        logger.info("redis host is " + redisHost);
        var pool = new JedisPool(
                new JedisPoolConfig(),
                redisHost
        );
        logger.info("redis pool loaded");

        get("/", (req, res) -> {
            registry.meter("hit").mark();
            var timer = registry.timer("timer").time();
            logger.info("hit from " + req.ip());
            String counter;
            try (var jedis = pool.getResource()) {
                counter = Long.toString(jedis.incr("counter"));
            } catch (Exception e) {
                logger.error("no redis", e);
                counter = " REDIS_OFFLINE";
            }
            var out = "<h1>" + hostName + "</h1>" +
                    "<h2> count is " + counter + "</h2>" +
                    getenv().entrySet().stream()
                            .map(e -> e.getKey() + ":" + e.getValue()).collect(joining("<br>"));
            timer.stop();
            return out;
        });
        logger.info("accepting connections");
    }

}
