#!/bin/bash

set -x 
set -e

# explicar arquitetura server / client da engine docker
# explicar a relação das variaveis de ambiente DOCKER_XXX com isso 
# mostrar "pipe" padrão e por que não funciona sem as variáveis
# mostrar que o "docker quick start" seta estas variáveis 
# explicar diferença win/linux (socket na VM vs pipe direto)
# explicar o porque do docker-machine (ele gerencia acesso além de criar máquinas)

CREATE_MACHINE() {

    # docker-machine works officially with:
    # - Amazon Web Services
    # - Microsoft Azure
    # - Digital Ocean
    # - Exoscale
    # - Google Compute Engine
    # - Generic (our own bare metal machines)
    # - Microsoft Hyper-V
    # - OpenStack
    # - Rackspace
    # - IBM Softlayer
    # - Oracle VirtualBox
    # - VMware vCloud Air
    # - VMware Fusion
    # - VMware vSphere

    MACHINE_NAME=$1
    docker-machine rm -f $MACHINE_NAME
    docker-machine create \
        --driver virtualbox \
        --virtualbox-cpu-count 1 \
        --virtualbox-disk-size 4096 \
        --virtualbox-memory 1024 \
        $MACHINE_NAME

    # no caso dos servidores de nuvem teriamos já magicamente accesso e ip já configurados
    # temos um docker host instalado pronto para diretamente rodarmos containers ou para 
    # fazer parte de um swarm  

    # conecta nossas ferramentas de client a máquina     
    # depois de conectado todos os comandos "docker XXX" vão para a máquina em questão
    eval $(docker-machine env $MACHINE_NAME --shell bash)
}

CREATE_MACHINE m1
CREATE_MACHINE m2
CREATE_MACHINE m3
