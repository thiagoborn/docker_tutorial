#!/usr/bin/env bash

set -x

DIR="c:/ProgramData/Docker/Config"
mkdir -p ${DIR}
echo "{\"insecure-registries\" : [\"192.198.10.252:5000\"]}" > ${DIR}/daemon.json
