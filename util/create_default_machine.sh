#!/usr/bin/env bash

docker-machine rm -f default
docker-machine create --virtualbox-memory 1024 \
    --engine-insecure-registry 192.168.10.252:5000 \
    default
