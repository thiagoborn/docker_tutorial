#!/bin/bash

set -x 
set -e

# conecta no dev
eval $(docker-machine env monster --shell bash)

DEV_IP=$(docker-machine ip monster)

# explain networks 
# cria rede 
docker network create tutorial-net || true
docker network ls

docker rm -f redis || true
# roda redis 
docker run -d \
 --network tutorial-net \
 --publish 6379:6379 \
 --name redis \
 redis:3.2.11

docker rm -f graphite || true
# roda graphite 
# estamos expondo a porta 80 por que queremos ver o dashboard funcionando
docker run -d \
 --network tutorial-net \
 -p 81:80 \
 -p 2003-2004:2003-2004 \
 -p 2023-2024:2023-2024 \
 -p 8125:8125/udp \
 -p 8126:8126 \
 --name graphite \
 graphiteapp/graphite-statsd

docker rm -f app_dev || true
# run our java app
docker run -d \
 --env-file env-server/dev.ini \
 --network tutorial-net \
 --publish 80:4567 \
 --name app_dev \
 ${DOCKER_REGISTRY}/env-server

# conecta no master
eval $(docker-machine env m1 --shell bash)
# lista todas as máquinas no swarm (só funciona no master)
docker node ls   
