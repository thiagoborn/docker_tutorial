class: middle, center

# Um oferecimento de:

![](img/fontec_logo.png)

---

class: middle, center

# Curso básico de:
![](img/swarm_logo.jpg)
## Docker Swarm
 
---

class: middle, center

# Introduzindo:

## Swarm
## Stack
## Services

---

## Prerequisitos

Precisamos alinhar que já estamos (pelo menos) familizarizados com:
- Docker (o que é)
- Docker (o que faz)
- Docker (como funciona)

---

## Relembrando Docker

- Os containers acham que tem sua própria rede, disco, lista de processos e sistema operacional (menos a kernel)
- isola as aplicações da nossa INFRA-ESTRUTURA. O sistema operacional do "host" não importa mais desde que rode docker
- acaba com o "dependency hell" na máquina host (ex: quase impossivel de ter duas versões do runtime de Erlang instaladas ao mesmo tempo, entre 
outros exemplos)
- podemos ter aplicações que tem dependências CONFLITANTES sem problemas (um roda com lib versão X e outra com lib versão Y)
- para-se de instalar dependências na máquina HOST (nem JDK, nem nada, diminuindo o ponto de contato de segurança)
- empacota explicitamente (e declarativamente, escrito em um .dockerfile) TODAS as dependencias de um app (toda as lib nativas, JDKs, node runtime, apt-gets, erlang runtime, etc)
- tem um formato de imagem que representa app empacotado no container com SO com tudo (tipo um arquivo .iso)
- é possivel deixar o ambiente de dev, homologação e produção muito parecidos
- cria uma interface comum para rodar/parar/agendar/configurar vários tecnologias como java,c,c++,erlang,node.js,rust,python,etc..

---

### Quando usar?

- Quando já se usa docker e containers :p (obviamente)
- Quando uma máquina já não é o suficiente para lidar com a carga do sistema
- Quando o script de deploy já tem 1000 linhas e não sabe lidar com desastres e faz tudo pela metade
- Quando digitar 'ssh xxx@um_ip' para dezenas de máquinas vira um desperdicio gigante de tempo
- Quando se tem uma quantidade grande de ips para decorar :p
- Quando olhar os logs se torna um grande problema "em qual máquina o log do serviço X está?"
- Quando se tem alguma máquina "central" que controla a saúde do sistema (helth-check, script de orquestração) e se quer que este sistema seja resitente a falhas e desastres
- Quando escalar o sistema é sinônimo de dor. Adicionar/Remover um nó/serviço geralmente significa um processo trabalhoso e várias etapas manuais
- Quando alguém pergunta "mas o sistema é feito de quais partes?" e não se sabe responder pq isso não está escrito em lugar nenhum :p (docker-compose.yml descreve toda a arquitetura em um formato simples)
- Quando não se tem muito idéia de qual serviço roda em qual máquina porém sabe-se a porta do mesmo (no swarm a porta é publicada em todos os nós)

---

### Quando usar? [2]
- Quando responder a pergunta "o que está rodando aonde?" significa logar em muitas máquinas e descobrir "by yourself" (swarm lista tudo isso)
- Quando se escreve IPs fixos no código fonte :( e se um serviço mudar de localização se tem que re-compilar ou re-deployar com nova config (swarm tem discovery/DNS embutido)
- Quando se precisa de um balanceador de carga robusto para os serviços (swarm balanceia automaticamente)
- Quando se precisa fazer updates que não parem o sistema como um todo (rolling updates, x máquinas por vez, etc)

---

## Docker Swarm
### No que ajuda?

- Ajuda a lidar com sistemas distribuidos. Várias máquinas / Vários nós
- Cada serviço facilmente acha o outro sem se utilizar de ips fixos e dns externos
- Balanceia/roteia as conexões a cada serviço sem ter que programar nada 
- Escala os serviços facilmente com um commando
- Isola os serviços em redes definidas (ex: frontend/backend)
- Trafega segredos como senhas/chaves seguramente via SSL e cryptografados no disco
- Faz "rolling updates" facilmente. Deve-se definir o tempo entre updates e o numero máximo de containers afetados 

---

### O que está incluido no pacote?
- Gerência do cluster: adicionar/remover máquinas, configurar se a máquina é manager ou worker
- Orquestração descentralizada e tolerante a falhas: o orquestrador funciona baseado em um quórum das máquinas manager, se existe uma MAIORIA dos nós manager o sistema ainda consegue cumprir todas suas funções de gerenciar os serviços
- TLS por padrão em todas as conexões entre o cluster
- Service discovery: DNS interno
- Balanceamento de conexões entre máquinas

---

class: middle, center
![](img/swarm_overview.png)
 
---

### Pedras no caminho
- Alguns problemas de sistemas distribuidos nunca vão embora como consistencia eventual e "aonde vão os dados/estado?"
- Por isso tudo que "salva no disco" é especialmente problemático (isto para QUALQUER sistema distribuído, não é uma caracteristica específica do docker-swarm)

---

## O que é o Swarm?
- Multiplas máquinas rodando a engine docker em modo swarm
- As máquinas podem ser "manager" (participa do grid de gerenciadores) ou "worker" (executa tasks) ou ambos 
- Se alguma máquina worker parar o sistema automaticamente agenda as tarefas que estavam rodando na máquina em outra 
- Se alguma máquina manager parar o sistema continua agendando tarefas normalmente caso ainda exista uma maioria de managers 
- Uma tarefa é um container rodando sob supervisão do orquestrador ao contrário de um container executado usando "docker run"
- Quando uma configuração de qualquer serviço muda, não é necessário parar/re-começar o serviço, o orquestrador faz isso automaticamente  
- Em modo swarm ainda é possivel rodar containers "standalone" ele só não vai ser "observado" pelo gerenciador

---

## Nós (a.k.a. Máquinas)
- Um nó é uma instância do docker-engine participando do swarm, é possivel rodar mais que um nó na mesma máquina física, porém altamente não recomendado, então geralmente um nó é igual a uma máquina
- Os nós manager são os que recebem uma definição de serviço e garantem o estado do mesmo no cluster
- Os nós manager elegem um líder e este é o nó que todas as requisições de orquestração são enviadas
- Se o lider morrer, os managers automaticamente elegem um novo líder (caso ainda exista uma maioria de managers)
- Os nós worker recebem e executam tarefas dispachadas pelo nó manager líder, e "conversa" constantemente com o líder para manter o estado desejado do sistema

---

class: middle, center
![](img/nodes_overview.jpg)

---

## Serviços e Tarefas
- Um serviço é a definição de tarefas para serem executadas nos nós worker, é o fundamento central do sistema swarm
- Quando criamos um serviço se define de qual IMAGEM ele deve ser executado
- Se o serviço for **replicado** o orquestrador define um número específico de réplicas que devem rodas nos nós worker
- Se o serviço for **glocal** o orquestrador roda uma tarefa por nó do swarm
- Uma terefa é a unidade atômica de agendamento do swarm, o orquestrador define um nó para cada tarefa seguindo as definiçoes de réplicas
- Tarefas depois de começadas nunca mais são movidas (a não ser que ela falhe)

---

## Balanceamento de carga automágico

- Quando um serviço publica uma porta ele a expõe em TODOS os nós/máquinas
- O Swarm usa o DNS interno para automaticamente rotear conexões e distribuir as requisições 

![](img/routing_mesh.png)

---

## Dev-Ops

### Criando o grid
- logue nas máquinas
- instale o docker
- configure o registro 

Escolha um nó para "bootar" o grid e rode:
``` 
docker swarm init --advertise-addr ${MY_EXTERNAL_IP} 
```
Após este comando esta engine docker estará rodando em modo "swarm"

---

## Dev-Ops
### Adicionando nós

Para adicionar outros nós a este grid é necessário um TOKEN de autenticação.
Para conseguir o token de autenticação digite em uma máquina manager o seguinte comando:

Token manager:
``` 
docker swarm join-token --quiet manager
```
Token worker: 
``` 
docker swarm join-token --quiet worker
```

Após anotar os tokens, acesse os outros nós e digite (utilize o token correpondente ao tipo de nó que vc quer: manager ou worker)
``` 
docker swarm join --token ${TOKEN} ${MANAGER_IP}
``` 
---

## Review Comandos Docker [1]

### Core 

| o que vc quer? | comando |
| :---- | ---: |
| ver informações sobre a engine | docker info |
| versão | docker version |

### Images

| o que vc quer? | comando |
| :---- | ---: |
| listar imagens | docker images | 
| contruir uma imagem | docker build | 
| baixar uma imagem | docker pull | 
| subir uma imagem | docker push |
| remover uma imagem | docker rmi | 

---
## Review Comandos Docker 
### Containers [1]

| o que vc quer? | comando |
| :---- | ---: |
| rodar comando dentro do container | docker exec | 
| ver stdout/stderr de um container | docker logs | 
| ver stdout/stderr de um container e faz proxy de signals | docker attach | 
| copiar arquivos do host para o container | docker cp | 
| qual portas o container ouve e exporta | docker port | 
| listar os containers | docker ps |
| renomear container | docker rename |
| começar container parado | docker start |
| recomeçar container | docker restart |
| remover container | docker rm | 
| parar container | docker stop |

---
## Review Comandos Docker
### Containers [2]

| o que vc quer? | comando |
| :---- | ---: |
| cria container e roda | docker run | 
| criar container sem rodar | docker create | 
| mostra cpu/mem/net/io do container em tempo real | docker stats | 
| rotular container | docker tag | 
| info detalhadas do container | docker inspect |
| lista processos do container | docker top | 
| bloqueia esperando container terminar | docker wait | 
| mostrar mudanças no filesystem em relação a imagem | docker diff | 

---

## Operando serviços **distribuídos** no grid

