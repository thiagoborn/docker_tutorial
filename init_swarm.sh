#!/bin/bash

MASTER_IP=$(docker-machine ip m1)
eval $(docker-machine env m1 --shell bash)
# cria node inicial master do swarm 
docker swarm init --advertise-addr $MASTER_IP
# pega a senha para os workers se juntarem 
TOKEN=$(docker swarm join-token worker -q)

eval $(docker-machine env m2 --shell bash)
docker swarm join --token $TOKEN $MASTER_IP:2377

eval $(docker-machine env m3 --shell bash)
docker swarm join --token $TOKEN $MASTER_IP:2377

