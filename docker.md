class: middle, center

# Um oferecimento de:

![](img/fontec_logo.png)

---

class: middle, center

# Curso básico de:
![](img/docker_logo.png)
 
---


class: middle, center

# Introduzindo:

## Docker 
## Docker-Compose
## Docker-Machine 
## Docker-Swarm

---

## O que faz?

- isola de aplicações a rede, disco, processos e sistema operacional (menos a kernel) do host (chamamos isso de "um container")
- no fim isola as aplicações da nossa INFRA-ESTRUTURA. Podemos ter o docker engine rodando em FreeBSD, Debian, Ubuntu, e não precisamos nos --
importar em qual SO host vamos "deployar" nosso app
- acaba com o "dependency hell" na máquina host (ex: quase impossivel de ter duas versões do runtime de Erlang instaladas ao mesmo tempo, entre 
outros exemplos)
- podemos ter aplicações que tem dependências CONFLITANTES sem problemas (um roda com lib versão X e outra com lib versão Y)
- para-se de instalar dependências na máquina HOST (nem JDK, nem nada, diminuindo o ponto de contato de segurança)
- empacota explicitamente (e declarativamente, escrito em um .dockerfile) TODAS as dependencias de um app (toda as lib nativas, JDKs, node runtime, apt-gets, erlang runtime, etc)
- tem um formato de imagem que representa app empacotado no container com SO com tudo (tipo um arquivo .iso)
- deixa o ambiente de dev, homologação e produção iguais 
- cria uma interface comum para rodar/parar/agendar/configurar vários tecnologias como java,c,c++,erlang,node.js,rust,python,etc..

---

## Táaaaaa mas o que faz, tecnicamente falando? [2]
- cria um "disco virtual" para o container 
- não permite que os processos do container vejam/acessem/alterem processos do host ou de outros containers
- não permite que containers aloquem mais memória do que o permitido
- não permite que containers usem mais CPU do que o permitido
- proteje o host (até um certo ponto) de que containers escrevam no seu sistema de arquivos
- criar networks e só permite container anexados a esta network a se comunicarem
- seletivamente permite os containers se anexar a portas do host

---

## Docker engine
- o que chamamos de "docker engine" é uma aplicação CLIENTE / SERVIDOR
- o cliente e o server PODEM OU NÃO estar rodando na mesma máquina 
- este daemon gerencia o que chamamos de "objetos docker" -> imagens, containers, networks e volumes
- o daemon, também chamado de app server, pode ser rodado através do comando "dockerd"
- a engine tem uma API Rest (que é a única forma de se comunicar com o daemon)
- um app de LINHA DE COMANDO (o cliente) que sabe se comunicar com o daemon usando a API Rest
- muitas aplicações usam a API Rest para gerenciar o daemon. ex: Kubernete / Swarm / Consul / Etcd 

---

class: middle, center

## Docker Engine 
### Estrutura

![](img/engine_structure.png)

---

## Docker client 
- é o comando "docker" (docker-compose e docker-machine são OUTROS softwares)
- o damon cujo o cliente está apontando é controlado através de variáveis de ambiente
- a variável DOCKER_HOST é um url que aponta para o daemon. ex: tcp://192.168.10.252:2376
- a variável DOCKER_CERT_PATH aponta para um arquivo de chave para protejer as comunicações com o daemon
- o comando "docker-machine env [nome_da_máquina]" gera um script com comandos que atribui valores a estas variáveis
- use "eval (docker-machine env monster)" para executar este script e fazer o client 'apontar' para a máquina em questão

---

## Tecnologia "under the hood"
- Namespaces (isola processos)
- Control Groups (limita recursos: processador, io, etc) 
- Union File System (layers tipo commits do git)
- Container Format (desenvolvido pela docker inc)
- na teoria tudo que o docker faz já estava disponivel para usuários de sistema linux fazem décadas
- docker "juntou" tudo em uma interface e formatos padrão

---

## Containers
- é para ser uma nova unidade de distribuição e TESTE de nossos apps
- é um ambiente isolado para nossos apps
- internamente pode ser qualquer SO baseado em linux que conhecemos (Debian, Ubuntu, Slackware, Alpine, etc..) 
- ele não vê o disco, processos, rede nada do host
- é uma instância rodando de uma IMAGEM
- usado o cliente podemos criar, começar, parar e deletar containers 
- a engine de docker monitora UM PROCESSO que é o processo principal do container
- se o processo principal morrer, a engine mata todos os outros processos do container e o considera parado 

---

## Containers e Testes
- qualquer container depois de inicializado e ter modificado o disco pode ser "commitado" e virar uma IMAGEM 
- pode-se entrar no software, criar contas fazer um snapshot e re-utilizar esta imagem pré-concfigurada para testar
- pode-se criar situações de erro e tirar um snapshot da aplicação logo neste momento, e re-encenar o erro várias vezes

---

## Por que containers são considerados "lightweight"
- comparando VMs com containers para distribuição de apps temos algumas vantages
- kernel COMPARTILHADA (várias kernels = mais overhead)
- memória COMPARTILHADA (não é necessário pré-alocar memória como em VMs)
- sem o tempo de levantar de uma máquina virtual
- sem o tempo de levantar um SO completo
- não precisam do overhead de um hypervisor (software de controle de VMs) 

---

class: middle, center

## Distribuindo apps: Containers vs VMS

![](img/containers_vs_vms.png)

---

## Imagem
- são "read-only"
- é criada usando um "dockerfile"
- é sempre baseada em outra imagem com algumas customizações
- pode se usar imagens dos registros ou contruir novas
- cada instrução no dockerfile é UMA LAYER na imagem 
- cada layer é um BLOB de deltas de dados + um digest 
- layers minimizam drasticamente a quantidade de dados que se move entre ambientes, pois todas cujo o digest é igual não são copiadas nem transmitidas
- toda vez que se contrói uma imagem layers que não mudaram não são recostruidas e também não são enviados para registros (caso o registro já tenha essa layer)
- o sistema de layers pode se traduzir em um TEMPO MENOR de entrega de apps com dependências gigantes 
- no caso de apps java incluimos as dependências em uma layer e nosso código em outra (não é necessário gerar um fat-jar)

---

### Workflow com containers
- escrevemos código e compartilhamos com colegas usando containers
- testers ou devs usam o mesmo container para empurar no ambiente de homologação e executar testes automáticos e manuais
- bugs são achados -> fix -> cria novo container -> re-compartilha com a empresa
- sem bugs -> "docker run nosso-app:nova-versão" no ambiente de produção 

---

### Uniformidade em rodar QUALQUER app (Para os testers)
- código nativo? "docker run my-native-app"
- node.js? "docker run my-node-app"
- java? "docker run my-java-app"
- erlang? "docker run my-erlang-app"
- php? "docker run my-php-app"

---

## Registros
- Guarda IMAGENS
- DockerHub é pré-configurado como registro padrão (assim como o maven-central.com é o padrão do maven)
- os comandos "docker run TAG" e "docker pull TAG" fazer o daemon baixar de um registro
- a forma que TAG é escrita dita DE ONDE vai vir a image -> my-app (dockerhub), thiagoborn/my-app (github), mycompany.com:5000/my-app (private registry)

---

## Dockerfile
- é o arquivo com a sequência de comandos que vai montar nossas imagens
- é executado a partir de um comando "docker build [context_dir]"
- '#' é um comentário
- instruções tem o formato "COMMAND ARGUMENT"
- lembrando que CADA COMANDO VIRA UM LAYER na imagem (com as mudanças que o comando causou no disco)
- PRECISA começar com um comando 'FROM'
- FROM: indica a imagem base. ex: debian:8.10 , postgres:9.6 , tomcat:7
- RUN: roda um commando (ou sequência de comandos) ex: apt-get install git , rm /home , etc (qualquer comando linux na verdade)
- EXPOSE: exporta portas (faz com o que o host consiga ver esta porta)
- WORKDIR: afeta o diretório que comandos como "RUN" vão executar
- ENV: adicionar uma variável de ambiente
- ADD: adiciona arquivos a imagem (e descopacta se necessário)
- COPY: adiciona arquivos a imagem
- ENTRYPOINT: diz qual o comando que "roda" essa imagem. ex: "java -jar webtorneios.jar"  (este processo define se o container está vivo ou não)
- ARG: define uma variável para ser passada em tempo de build (no comando 'docker build')
- outros comandos avançados: ONBUILD, STOPSIGNAL, HEALTHCHECK, SHELL 

---

## Dockerfile 
### Exemplo java 10

```
FROM openjdk:10.0.1-10-jre
COPY target/lib/* /lib/
COPY target/env-server.jar /bin
EXPOSE 4567
ENTRYPOINT java -ja /bin/env-server.jar
```

---

## Dockerfile 
### Exemplo Node
```
FROM ubuntu:17.10
RUN apt-get install -y software-properties-common python
RUN add-apt-repository ppa:chris-lea/node.js
RUN echo "deb http://us.archive.ubuntu.com/ubuntu/ precise universe" >> /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y nodejs=0.6.12~dfsg1-1ubuntu1
RUN mkdir /var/www
ADD app.js /var/www/app.js
ENTRYPOINT node /var/www/app.js
```

---

## Arquivo '.dockerignore'
- quando se roda o comando "docker build" o diretório de contexto INTEIRO é enviado pelo cliente para o daemon
- lembrese que o daemon pode ser REMOTO (e a intenet pode ser ruim)
- por isso as vezes é necessário indicar quais diretórios e arquivos você quer q sejam ignorados
- o formato é igual ao do .gitignore e do .svnignore

---

## .dockerignore
### Exemplo Webtorneios

```
out
truco2
truco3
truco4
**/ios_*
**/Assets
**/Library
**/ProjectSettings
```

---

## Uso básico do Docker
digamos que vamos lançar a versão 666 de um app

> cd app
> ./build_the_app.sh

contruindo a imagem e dando a ela o nome "registry.company.com:5000/tutorial_app:666"
lembrando que "registry.company.com:5000" é o domínio e a porta do registro privado da empresa
e "tutorial_app:666" é o nome do app e a versão do mesmo
> docker build . --tag registry.company.com:5000/tutorial_app:666

empurra essa imagem para o registro da empresa
> docker push registry.company.com:5000/tutorial_app:666

rodando essa imagem na máquina de desenvolvimento
> docker run registry.company.com:5000/tutorial_app:666

---

## Volumes
- é possivel linkar pastas dentro do container para um volume 
- estes volumes podem ser compartilhados entre containers na MESMA ENGINE
- volumes podem ser gerenciados (criados/deletados/nomeados/configurados) usando o cliente (comando "docker volume")
- existem "drivers" de volume que podem criar volumes que são compartilhados entre máquinas (usando aws/etc..)

---

## Uso e vantagens de velocidade de deploy/build no caso de JAVA 

Digamos que temos uma aplicação java que tem várias dependências e faz um request http em um url e posta o resultado em outro url 

Um dockerfile para este seria o seguinte:
```
FROM openjdk:8-jre
COPY target/lib/* /lib
COPY target/app.jar /bin
ENTRYPOINT java -jar /bin/app.jar
```

lembrando que CADA comando deste gera um LAYER na imagem.

note que:
- não geramos um FAT-JAR (o que em alguns casos por ser MUITO mais rápido, tipo websites de 400mb)
- ajustamos o POM para que as dependências sejam copiadas para target/lib
- target/app.jar é APENAS as classes de nosso app

---

## Uso e vantagens de velocidade de deploy/build no caso de JAVA [2]

a partir do momento que temos que gerar várias versões do nosso app, temos um ganho de velocidade em pelo menos 3 situações distintas:
- na hora de contruir a imagem (a layer das libs, que muda muito pouco, só é contruida 1 vez)
- na hora de empurrar ela no registro (o registro não precisa receber as libs, pois ele sabe que já tem aquela layer em específico)
- na hora de fazer download da imagem (o cliente não faz download do layer das libs, pois sabe que já tem o mesmo)

---

## Comandos comuns do docker
- docker images | mostra todas as images no cache local (do daemon que vc está conectado)
- docker image rm [image-name] | deleta uma imagem do disco
- docker tag [source] [destination] | "taggeia" uma imagem (na verdade copia e dá um novo nome)
- docker commit | cria uma imagem a partir do disco de um container (snapshot)
- docker pull [image-name] | baixa uma imagem para o cache local
- docker push [image-name] | faz upload da imagem para o registro
- docker build [context-dir] | contrói uma imagem usando um diretório de contexto
- docker run [image-name] | cria um container a partir de uma imagem 
- docker stop [container-name] | para um container
- docker start [container-name] | começa um container (tem q ter sido criado antes)
- docker rm [container-name] | remove um container (apaga do disco)
- docker ls | mostra todos os container rodando
- docker ls -a | mostra todos os containers (inclusive os parados)

---

## Executando Containers (docker run)
para rodar um container temos que especificar uma IMAGEM que você quer executar (docker run [IMAGEM])
podemos passar opções de:
- foreground / background execution
- identificação do container (--name)
- politica de execução e re-execução (unless-stopped, always, on-failure, etc)
- parâmetros de rede (--network, --add-host, --ip, etc)
- limitações de runtime como CPU / memória e largura de banda de IO (--memory, --cpu-shares, --cpus)
- persistência do layer de escrita (--rm) 

---

## Vantagens no Continuous Delivery
- Dá para levantar TODAS as ferramentas que vão compilar um container DENTRO de um container 
- Isso libera a máquina do CI de precisar ter QUALQUER sdk e runtime instalados
- builds de multi-estágios fazer tudo funcionar com um simples "docker build"

---

## Docker Compose
- orquestra CONTAINERS (cordenação entre vários containers)
- definido usando um arquivo docker-commpose.yaml 
- podemos definir serviços / redes / imagens (ver exemplos WT)
- praticamente todas as opções que podemos passar em "docker run" tem uma opção relativa no compose
- podemos levantar arquiteturas inteiras com apps, banco de dados, caches, proxys, tudo na máquina do dev com um único comando "docker-compose up"

---

## Docker Compose
### Exemplo

```
version: "3"
services:
  redis:
    image: redis:3.2.11
  app:
    image: $REGISTRY_IP:5000/env-server    
    ports: 
      - "80:4567"

```

---

#### Docker Compose - Exemplo Homologação WebTorneios

```
version: '3'
services:
  static:
    image: ${DOCKER_REGISTRY}:5000/webtorneios-static:${BUILD}
    ports:
      - '10080:80'
  metrics:
    image: sitespeedio/graphite
    ports:
      - '10020:80'
    expose:
      - '2003'
  db:
    image: ${DOCKER_REGISTRY}:5000/webtorneios-db:${BUILD}
    ports:
      - '10010:5432'
  memory:
    image: redis
    ports:
      - '10011:6379'
  frontend:
    env_file:
      - '../../cfg/homologation.ini'
    image: ${DOCKER_REGISTRY}:5000/webtorneios-frontend:${BUILD}
    ports:
      - '10000:9000'
      - '10001:9001'
      - '10081:8080'
    expose:
      - '10666'
  backend:
    env_file:
      - '../../cfg/homologation.ini'
    image: ${DOCKER_REGISTRY}:5000/webtorneios-backend:${BUILD}
```

---

#### Docker Compose - Exemplo Ambiente de desenvolvimento WebTorneios

```
version: '3'
services:
  db_felipe:
    image: ${DOCKER_REGISTRY}:5000/webtorneios-db:${BUILD}
    ports:
      - 20000:5432
  db_born:
    image: ${DOCKER_REGISTRY}:5000/webtorneios-db:${BUILD}
    ports:
      - 30000:5432
  db_ivan:
    image: ${DOCKER_REGISTRY}:5000/webtorneios-db:${BUILD}
    ports:
      - 40000:5432
  memory_felipe:
    image: redis
    ports:
      - 20001:6379
  memory_born:
    image: redis
    ports:
      - 30001:6379
  mysql:
    image: mysql
    environment:
      MYSQL_ROOT_PASSWORD: fontec
    ports:
      - '30010:3306'
```

---

## Criando um ambiente de desenvolvimento
- com o compose é possivel levantar facilmente um ambiente de desenvolvimento (e homologação) na máquina dos desenvolvedores/homologação
- criar e "linka" database, caches, proxys, qualquer coisa

---

## Docker Machine 
provisiona máquinas com a engine de docker:
- podem ser máquinas com um so já instalado
- ou máquinas que o próprio machine vai criar no virtual-box (e afins) ou no cloud
- nos dá uma API comum para usarmos com inúmeros serviços de cloud

---

## Docker Machine [2]

funciona oficialmente com:
- Amazon Web Services
- Microsoft Azure
- Digital Ocean
- Exoscale
- Google Compute Engine
- Generic (our own bare metal machines)
- Microsoft Hyper-V
- OpenStack
- Rackspace
- IBM Softlayer
- Oracle VirtualBox
- VMware vCloud Air
- VMware Fusion
- VMware vSphere

e tem plugins (não oficiais) para vários outros datacenters

---

## Docker Swarm
- podemos juntar vários daemons de docker e fazer-los funcionar como um único ambiente
- um swarm tem nós master (participam do log criptografado de RAFT) e nós worker (que só executam tasks)
- o modo swarm não impede de maneira alguma que rodemos containers de forma isolada em cada nó (só containers gerenciados pelo swarm são afetados)
- cria o conceito de serviços (podem ser "replicados", cujo escala até o número informado pelo dev-ops ou globais que roda um task por nó)
- gerencia um "cluster" de engines de docker criando um "swarm" aonde podemos rodar containers e serviços
- o swarm se adapta para manter o número de task para o serviço desejado (inclusive lidando com falha de nós do swarm)
- serviços dentro de um swarm podem ser descobertos por DNS 
- balanço automático de entrada (serviços que exportam a porta 80, podem ser acessados por QUALQUER NÓ do swarm)
- rolling updates

---

## Docker Swarm

comandos: 
- docker swarm init
- docker swarm join

comandos (no master): 
- docker node ls
- docker service create
- docker service inspect
- docker service ps
- docker service scale 
- docker service update

---

## Docker Stack 
- é uma coleção de services (ele é para o swarm o que o docker-compose é para o docker)
- usa por padrão o formato de arquivo do docker-compose (tudo q vc aprendeu no compose vale para serviços do swarm)

comandos (a partir de um docker-compose file):
- docker stack deploy

---

### Docker Configs
- distribui configurações no swarm
- monta em um arquivo no root (dá para mudar usando a opção target)

ex:
> echo "minha config" | docker config create my-config 
monta um arquivo com o conteúdo "meu segredo" em /my-config de TODOS os containers que explicitamente pediram esta config

pode usar arquivos tmb:
> docker config create my-config configfile.ini

comandos:
- docker config create
- docker config inspect
- docker config ls
- docker config rm

---

### Docker Secrets
- distribui segredos (configurações como senhas) de forma cryptografada no swarm
- funciona basicamente da mesma maneira que os configs porém em cima de uma camada TLS 
- monta em /run/secrets/[nome_do_segredo]
- útil para: nomes de usuário e passwords de banco de dades e caches, certificados e chaves, ssh keys, etc

comandos:
- docker config create
- docker config inspect
- docker config ls
- docker config rm

---

class: middle, center

# Thank You

![](img/docker_whale.jpg)