#!/bin/bash

set -x 
set -e

# conecta no dev
eval $(docker-machine env monster --shell bash)

# explain tag magic
TAG="${DOCKER_REGISTRY}/env-server"

# lets build out image
pushd env-server

# let maven do it's magic
mvn package
# build the image (see Dockerfile for reference) and tag it
docker build --tag $TAG .
# push this image to the registry we created in the INFRA machine
docker push $TAG

popd

